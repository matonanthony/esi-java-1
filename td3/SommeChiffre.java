import java.util.Scanner;

public class SommeChiffre { 
	public static void main ( String[] args ) { 
		 int nbrBase;
		 int nbrUni;
		 int nbrDiz;
		 int nbrCen;

		 Scanner clavier = new Scanner ( System.in ) ;
		 nbrBase = clavier.nextInt();
		 nbrCen = nbrBase / 100;
		 nbrDiz = (nbrBase % 100) / 10;
		 nbrUni = nbrBase % 10;
		 System.out.println(nbrCen + nbrDiz + nbrUni);
	}
}

