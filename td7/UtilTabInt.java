package td7;
public class UtilTabInt { 
	public static void initialiser (int tab[]){
		for(int i = 0; i < tab.length; i++){
			tab[i] = i;
			System.out.println(tab[i]);
		}
	}
	public static void afficher (int tab[]){
		for(int i = 0; i < tab.length; i++){
			System.out.print(tab[i]+" ");
		}
	}
	public static int sommer ( int tab[]){
		int somme = 0;
		for(int i = 0; i < tab.length; i++){
			somme = somme + tab[i];
		}
		return somme;
	}

	public static int maximum (int tab[]){
		int max = 0;
		for(int i = 0; i < tab.length; i++){
			if(tab[i] > max){
				max = tab[i];
			}
		}
		return max;
	}

	public static void indiceMax (int tab[]){
		int max = maximum(tab);
		for(int i = 0; i < tab.length; i++){
			if(tab[i] == max){
				System.out.println(i);
			}
		}
	}
	public static int [] créer (int taille){
		int tabEntiers[] = new int[taille];
		return tabEntiers;
	}
	public static boolean estTrié (int tab[]){
		for(int i = 1; i < tab.length; i++){
			if(tab[i] < tab[i-1]){
				return false;
			}
		}
		return true;
	}

	// Actuellement non fonctionnel, renvoi l'adresse du tableau
	public static int [] créerEtInitialiser (int taille, int valeurDépart){
		int tabEntiers[] = new int[taille];
		for(int i = 0; i < tabEntiers.length; i++){
			tabEntiers[i] = valeurDépart;
		}
		return tabEntiers;
	}

	public static void renverser (int tab[]){
		int tmp;
		for(int i = 0; i < (tab.length / 2); i++){
			tmp = tab[i];
			tab[i] = tab[(tab.length / 2)+ 1-i];
			tab[(tab.length / 2)+ 1-i] = tmp;
			System.out.print(tab[i]+" ");
		}
	}
}
