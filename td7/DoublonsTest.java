package td7;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Tests for {@link Doublons}.
 *
 * @author 40103@heb.be (Maton Anthony)
 */
//@RunWith(JUnit4.class)
public class DoublonsTest {

	@Test
		public void thisAlwaysPasses() {
			int tab[] = {4,4,3,1,56};
			boolean attendu = true;
			boolean obtenu = Doublons.doublons(tab);

			assertEquals(attendu, obtenu);
		}
	@Test
		public void supposeFalse() {
			int tab[] = {0,1,3,4,5};
			assertFalse(Doublons.doublons(tab));
}			
	@Test
		@Ignore
		public void thisIsIgnored() {
		}
}
