package td6;
import java.util.Scanner;
public class SommeV3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);  
        int valeur = sc.nextInt();
        int somme = 0;

        while(valeur >= 0){
            somme = somme + valeur;
            valeur = sc.nextInt();
        }
        System.out.println(somme );
    }
}
