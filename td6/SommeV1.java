package td6;
import java.util.Scanner;

public class SommeV1  {
    public static void main(String[] args)  { 
        int nbValeurs;
        int valeur;
        int somme = 0;
        

        Scanner sc = new Scanner(System.in);
        
        nbValeurs = sc.nextInt();
        for(int i = 1; i <= nbValeurs; ++i){
            valeur = sc.nextInt();
            somme = somme + valeur;
        }

        System.out.println(somme);
    }
}


