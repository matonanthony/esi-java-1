package td6;
import java.util.Scanner;
public class SommeV2 {
    public static void main(String[] args) {
        boolean encore;
        int valeur;
        int somme = 0;
        Scanner sc = new Scanner(System.in);  
        do{
            valeur = sc.nextInt();
            somme = somme + valeur;
            encore = sc.nextBoolean();
        }while(encore);
        System.out.println(somme);
    }
}

        
