package td6;
public class SommeChiffres  {
    public static void main(String[] args)  {
        System.out.println(sommeChiffres(123));
    }
    public static int sommeChiffres(int chiffre){
        int somme = 0;
        int reste = chiffre;

        if(chiffre < 0){
            throw new IllegalArgumentException();
        }

        while(reste > 0){
            somme = somme + reste % 10;
            reste = reste / 10;
        }
        return somme;
    }
}
