package g40103.i2;
import evaluations.interro2.Cesar;
import java.util.Scanner;

public class ChiffreDeCesar {
    public static void main(String[] args){
        char[] message = Cesar.getMessage();
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez entrer le décalage : ");
        int decalage = sc.nextInt();
        System.out.println(message[5]);
        afficherCaractères(message);
        char[] code = new char[message.length];
        code = décoderMessage(message, decalage);
        System.out.println(code[0]);

        char[][] possibilite = décodage(code);
        for(int i = 0; i < 26; i++){
            System.out.println(possibilite[7]);
        }
    }
    public static void afficherCaractères(char[] code){
        for(int i = 0; i < code.length; i++){
            System.out.print(code[i]+" ");
        }
    }
    /**
     * Décode un message codé à l'aide du Chiffre de César
     * @param message char[]    Tableau à simple entrée contenant le message à 
     * décoder
     * @param decalage int      Contient le décalage entrer par l'utilisateur
     * pour décoder son message
     * @return code char[]      Retourne un tableau de caractères contenant le
     * message decodé de l'utilisateur
     */
    public static char[] décoderMessage(char[] message, int decalage){
        char[] code = new char[message.length];
        if(decalage < 0 || decalage > 25){
            throw new IllegalArgumentException("Décalage invalide");
        }
        for(int i = 0; i < message.length; i++){
            int tmp = message[i] + decalage;
            if(tmp > 90) {
                tmp = tmp - 26;
            }
            code[i] = (char)tmp;
        }
        return code;
    }
    public static char[][] décodage(char[] code){
        char[][] possibilite = new char[26][code.length];
        for(int i = 0; i < 26; i++){
            for(int j = 0; j < code.length; j++){
                possibilite[i][j] = décoderMessage(code, i)[j];
            }
        }
        return possibilite;
    }
}

