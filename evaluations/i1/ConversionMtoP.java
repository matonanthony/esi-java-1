import java.util.Scanner;
/**
 * Ce programme réalise la conversion de mètres en pieds et vice versa
 * en se basant sur la saisie de l'utilisateur
 *
 * @author Maton Anthony
 * @version 1.0
 * @param none
 *
 * */
public class ConversionMtoP  {
    /**
     * Méthode principale du logiciel
     * */
    public static void main(String[] args)  {
        Scanner sc = new Scanner(System.in);  
        double valeur;
        String unité;
        System.out.println("Entrez la mesure :");
        valeur = sc.nextDouble();
        affichageUnités();
        System.out.println("Entrez l'unité de mesure :");
        sc.nextLine();
        unité = sc.nextLine();
        conversion(valeur, unité);
    }
    /**
     * Méthode réalisant l'affichage de la syntaxe à entrer pour les unités
     * */
    public static void affichageUnités(){
        System.out.println("m/M = mètres, ft/FT = pieds");
    }
    /**
     * Méthode effectuant la conversion des pieds vers les mètres
     * @param unPieds
     *      La valeur à convertir
     * */
    public static double piedsToMètres(double unPieds){
        final double PIEDS_METRES = 0.3048;
        return PIEDS_METRES * unPieds;
    }
    /**
     * Méthode effectuant la conversion des pieds en mètres
     * @param unMètre
     *      La valeur à convertir
     * */
    public static double mètresToPieds(double unMètre){
        final double METRES_PIEDS = 3.2809;
        return METRES_PIEDS * unMètre;
    }
    /**
     * Test afin de déterminer si la mesure est étrange ou non
     * @param valeur
     *      La valeur à tester
     * */
    public static boolean mesureBizarre(double valeur){
        return valeur <= -1500 || valeur >= 1500;
    }
    /**
     * Méthode effectuant la conversion
     * @param valeur
     *         La valeur à convertir.
     * @param unité
     *         La chaîne de caractères indiquant l'unité cible de la conversion
     * */
    public static void conversion(double valeur, String unité){
        boolean unitéValide;
        switch (unité){
            case "m" :
            case "M" :
                System.out.println("Conversion en pieds de "+valeur+", mètres:"+mètresToPieds(valeur));
                unitéValide = true;
                break;
            case "ft" :
            case "FT" :
                System.out.println("Conversion en mètres de "+valeur+", pieds:"+piedsToMètres(valeur));
                unitéValide = true;
                break;
            default:
                System.out.println("Pas de conversion possible, unité inconnue.");
                affichageUnités();
                unitéValide = false;
        }
        if (unitéValide && mesureBizarre(valeur)){
            System.out.println("La mesure est bizarre : trop petite ou trop grande");
        }

    }

}
