public class Util { 
	public static void main ( String[] args ) { 
			System.out.println(nbJours(2,2012));
	}

	public static int nbJours(int mois, int annee){ 
		if(mois % 2 == 0 && (mois != 10 && mois != 2 )){
			return 30;
		}else if(mois == 2){
			if (estBissextile(annee) == true){
				return 29;
			}else{
				return 28;
			}
		}else{
			return 31;

		}
	}
	public static boolean estBissextile (int annee){
		if((annee % 4 == 0 && annee % 100 != 0) || (annee % 400 == 0)){
			return true;
		}else{
			return false;
		}
	}
}

