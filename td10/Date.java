package g40103.td10;

public class Date { 
	private int jour;
	private int mois;
	private int année;

	public Date(int unJour, int unMois, int uneAnnée){
		if(unJour<1 || unJour > Util.nbJours(unMois,uneAnnée) || unMois < 1 ||
			unMois > 12 || uneAnnée < 0) {
			throw new IllegalArgumentException("Date invalide");
		}
		jour = unJour;
		mois = unMois;
		année = uneAnnée;
	}
	public String toString(){
		return "*Le "+String.format("%02d",jour)+"/"+String.format("%02d",mois)+"/"+String.format("%04d",année)+"*";
	}
} 
