package g40103.gameover.model;
public class DungeonPosition {
	/**
	 * @licenses : MIT Licence
	 */
	public static int row;
	public static int column;
	
	private static final DungeonPosition P_BARBARIAN_1;
	private static final DungeonPosition P_BARBARIAN_2;
	private static final DungeonPosition P_BARBARIAN_3;
	private static final DungeonPosition P_BARBARIAN_4;
	
	static{
		P_BARBARIAN_1 = new DungeonPosition();
		P_BARBARIAN_2 = new DungeonPosition();
		P_BARBARIAN_3 = new DungeonPosition();
		P_BARBARIAN_4 = new DungeonPosition();
	}
	private DungeonPosition(){
		P_BARBARIAN_1.row = -1;
		P_BARBARIAN_1.column = 0;
		
		P_BARBARIAN_2.row = 5;
		P_BARBARIAN_2.column = 4;
		
		P_BARBARIAN_3.row = 1;
		P_BARBARIAN_3.column = 5;
		
		P_BARBARIAN_4.row = 4;
		P_BARBARIAN_4.column = -1;
	}
	
	public DungeonPosition(int ligne,int colonne) throws GameOverException{
		if(colonne < 0 || colonne > Dungeon.N - 1 || ligne < 0 || 			ligne > Dungeon.N - 1){
			throw new GameOverException("La position actuelle est hors"+       										   " du donjon. \n Erreur irrécupérable");
		}
			// Dungeon.N --> Variable N de la classe Dungeon.
			column = colonne;
			row = ligne;
	}
	public int getRow(){
		return row;
	}		
	public int getColumn(){
		return column;
	}
	public static DungeonPosition move(Direction direction) throws GameOverException{
		switch(direction){
			case UP : row = row + 1; break;
			case DOWN: row = row - 1; break;
			case LEFT: column = column - 1; break;
			case RIGHT: column = column + 1; break;
			default: throw new GameOverException("Veuillez entrer une donnée"+ 													"gérée"); 
		}
		return(new DungeonPosition(row, column));
	}
}
